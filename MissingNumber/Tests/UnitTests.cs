using MissingNumber;
namespace Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestCubicRootWithOneHundredTwentyFive()
        {
            string input = "3,1,4";
            string[] inputArray = input.Split(',');

            int[] intArray = new int[inputArray.Length];

            for (int currentIndex = 0; currentIndex < intArray.Length; currentIndex++)
            {
                intArray[currentIndex] = int.Parse(inputArray[currentIndex]);
                // Console.WriteLine(intArray[currentIndex]);
            }



            int expected = 4;

            int actual = MissingNumber.MissingNumber.FindMissing(intArray);

            Assert.AreEqual(expected, actual);
        }

        


    }
}